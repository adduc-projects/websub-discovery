<?php declare(strict_types=1);

namespace Adduc\WebSub;

use function GuzzleHttp\Psr7\parse_header;
use function GuzzleHttp\Psr7\uri_for;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class Discovery
{
    /** @var LoggerInterface */
    protected $logger;

    public function __construct(?LoggerInterface $logger = null)
    {
        if ($logger === null) {
            $logger = new NullLogger();
        }

        $this->logger = $logger;
    }

    public function discover(ResponseInterface $response): ?DiscoveryResult
    {
        if ($result = $this->checkHeaders($response)) {
            return $result;
        }

        if ($result = $this->checkBody($response)) {
            return $result;
        }

        return null;
    }

    protected function checkHeaders(ResponseInterface $response): ?DiscoveryResult
    {
        if (!$response->hasHeader('Link')) {
            return null;
        }

        $links = $response->getHeader('Link');

        $links = parse_header($links);

        $uris = [];

        foreach ($links as $link) {
            if (preg_match('/^<(.*)>$/', $link[0], $matches)) {
                $link[0] = $matches[1];
            }

            $rel = strtolower($link['rel']);
            $uri = uri_for($link[0]);
            $uris[$rel][] = $uri;
        }

        if ($uris['self'] ?? null || $uris['hub'] ?? null) {
            return new DiscoveryResult($uris['self'][0] ?? null, ...$uris['hub'] ?? []);
        }

        return null;
    }

    protected function checkBody(ResponseInterface $response): ?DiscoveryResult
    {
        $body = trim($response->getBody()->__toString());

        if (!$body) {
            return null;
        }

        $doc = new \DOMDocument();
        libxml_use_internal_errors(true);
        $doc->loadHTML($body, LIBXML_COMPACT | LIBXML_NONET);
        libxml_clear_errors();
        libxml_use_internal_errors(false);

        $xpath = new \DOMXpath($doc);
        $elements = $xpath->query("//link");

        $uris = [];

        foreach ($elements as $element) {
            $rel = $element->attributes->getNamedItem('rel');
            $href = $element->attributes->getNamedItem('href');

            if ($rel && $href) {
                $uris[$rel->value][] = uri_for($href->value);
            }
        }

        if ($uris['self'] ?? null || $uris['hub'] ?? null) {
            return new DiscoveryResult($uris['self'][0] ?? null, ...$uris['hub'] ?? []);
        }

        return null;
    }
}
