<?php declare(strict_types=1);

namespace Adduc\WebSub;

use PHPUnit\Framework\TestCase;
use function GuzzleHttp\Psr7\uri_for;
use GuzzleHttp\Psr7\Response;
use function GuzzleHttp\Psr7\stream_for;
use Adduc\WebSub\Discovery;
use Psr\Http\Message\ResponseInterface;

class DiscoveryTest extends TestCase
{
    /**
     * @dataProvider provideDiscover
     */
    public function testDiscover(ResponseInterface $response, ?DiscoveryResult $expected)
    {
        $result = (new Discovery())->discover($response);

        $this->assertEquals($expected, $result);
    }

    public function provideDiscover(): array
    {
        $single_hub = new DiscoveryResult(
            uri_for('https://example.com/feed'),
            uri_for('https://hub1.example.com/')
        );

        $multiple_hub = new DiscoveryResult(
            uri_for('https://example.com/feed'),
            uri_for('https://hub1.example.com/'),
            uri_for('https://hub2.example.com/')
        );

        $files = [
            'single-html' => stream_for(file_get_contents(__DIR__ . '/fixtures/single.hub.html')),
            'single-xml' => stream_for(file_get_contents(__DIR__ . '/fixtures/single.hub.xml')),
            'multiple-html' => stream_for(file_get_contents(__DIR__ . '/fixtures/multiple.hub.html')),
            'multiple-xml' => stream_for(file_get_contents(__DIR__ . '/fixtures/multiple.hub.xml')),
            'no-html' => stream_for(file_get_contents(__DIR__ . '/fixtures/no.hub.html')),
            'no-xml' => stream_for(file_get_contents(__DIR__ . '/fixtures/no.hub.xml')),
            'single-header' => require 'fixtures/single.header.php',
            'multiple-header' => require 'fixtures/multiple.header.php',
        ];

        $tests = [
            // Single hub in body
            [new Response(200, [], $files['single-html']), $single_hub],
            [new Response(200, [], $files['single-xml']), $single_hub],

            // Multiple hubs in body
            [new Response(200, [], $files['multiple-html']), $multiple_hub],
            [new Response(200, [], $files['multiple-xml']), $multiple_hub],

            // Hub in headers
            [new Response(200, $files['single-header']), $single_hub],
            [new Response(200, $files['multiple-header']), $multiple_hub],

            // Mixture (headers should be taken, body should be ignored)
            [new Response(200, $files['single-header'], $files['single-html']), $single_hub],
            [new Response(200, $files['single-header'], $files['single-xml']), $single_hub],

            [new Response(200, $files['single-header'], $files['single-html']), $single_hub],
            [new Response(200, $files['single-header'], $files['single-xml']), $single_hub],

            [new Response(200, $files['single-header'], $files['multiple-html']), $single_hub],
            [new Response(200, $files['single-header'], $files['multiple-xml']), $single_hub],

            // No Hub
            [new Response(200, [], $files['no-html']), null],
            [new Response(200, [], $files['no-xml']), null],
        ];

        return $tests;
    }
}
