<?php declare(strict_types=1);

namespace Adduc\WebSub;

use Psr\Http\Message\UriInterface;

class DiscoveryResult
{
    /**
     * @var UriInterface
     */
    public $canonical_uri;

    /**
     * @var UriInterface[]
     */
    public $hub_uris;

    public function __construct(?UriInterface $canonical_uri = null, UriInterface ...$hub_uris)
    {
        $this->canonical_uri = $canonical_uri;
        $this->hub_uris = $hub_uris ?? [];
    }

    public function setCanonicalUri(?UriInterface $canonical_uri): void
    {
        $this->setCanonicalUri($canonical_uri);
    }

    public function getCanonicalUri(): ?UriInterface
    {
        return $this->canonical_uri;
    }

    public function setHubUris(UriInterface ...$hub_uris): void
    {
        $this->hub_uris = $hub_uris ?? [];
    }

    public function addHubUri(UriInterface $hub_uri): void
    {
        $this->hub_uris[] = $hub_uri;
    }

    /**
     * @return UriInterface[]
     */
    public function getHubUris(): array
    {
        return $this->hub_uris;
    }
}
