<?php declare(strict_types=1);

return [
    'Link' => [
        '<https://hub1.example.com/>; rel="hub"',
        '<https://example.com/feed>; rel="self"',
    ]
];
